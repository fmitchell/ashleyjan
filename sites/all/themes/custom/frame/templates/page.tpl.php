<header class="<?php print $classes; ?>">
  <div class="header_wrapper container">
    <a href="<?php print $front_page; ?>" class="logo"><span>AshleyJan</span></a>

    <?php if (isset($main_menu)): ?>

      <?php print $main_menu; ?>

    <?php endif; ?>

  </div>
</header>

<?php if (drupal_is_front_page()): ?>
  <div class="fullscreen_block"></div>
<?php endif; ?>

<?php if (!drupal_is_front_page()): ?>
<div class="main_wrapper">
<!-- C O N T E N T -->
<div class="content_wrapper">
  <div class="page_title_block">
    <div class="container">
      <?php if ($title): ?>
      <h1 class="title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php if ($breadcrumb): ?>
      <div class="breadcrumbs"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div class="container">
    <div class="content_block <?php print $content_block; ?> row">
    <div class="fl-container <?php print $fl_container; ?>">
      <div class="row">
        <div class="posts-block <?php print $posts_block; ?>">
          <div class="contentarea">

            <div class="row-fluid">
              <div class="span12 module_cont <?php print $module; ?>">

                <?php print $messages; ?>
                <?php if (isset($tabs)): ?>
                  <div class="nav nav-tabs"><?php print render($tabs); ?></div>
                <?php endif; ?>
                <?php print render($page['help']); ?>
                <?php print render($page['content']); ?>

              </div>
            </div><!-- .row-fluid -->

            <?php if (($page['featured_works'])): ?>
            <div class="row-fluid">

              <div class="span12 module_cont module_feature_posts">

                <div class="bg_title"><h3 class="headInModule">Featured Works</h3></div>

                <div class="featured_slider">

                  <?php print render($page['featured_works']); ?>

                  <div class="clear"></div>

                </div><!-- .featured_slider -->

              </div><!-- .module_cont -->

            </div><!-- .row-fluid -->
            <?php endif; ?>

            <div class="row-fluid">
              <div class="span12 module_cont">
                <a href="javascript:history.back()" class="btn_back">Back</a>
              </div>
            </div><!-- .row-fluid -->

          </div><!-- .contentarea -->
        </div><!-- .posts-block -->

        <div class="left-sidebar-block span3"></div>

      </div><!-- .row -->

      <div class="clear"><!-- ClearFix --></div>

    </div><!-- .fl-container -->
    <div class="right-sidebar-block span3">
      <aside class="sidebar">

        <?php if ($right_sidebar): ?>
          <?php print render($page['sidebar_second']); ?>
        <?php endif; ?>

      </aside>
    </div><!-- .right-sidebar -->

    <div class="clear"><!-- ClearFix --></div>

    </div><!-- .content_block -->
  </div><!-- .container -->
</div><!-- .content_wrapper -->

</div><!-- .main_wrapper -->

<div class="pre_footer">
  <div class="container">
    <aside id="footer_bar" class="row">
      <div class="span6">
        <div class="sidepanel widget_posts">
          <div class="bg_title"><h3 class="sidebar_header">Latest Artwork</h3></div>
          <ul class="recent_posts">
            <?php print $latest_artwork; ?>
          </ul>
        </div><!-- .sidepanel -->
      </div>

      <div class="span6">
        <div class="sidepanel widget_twitter">
          <div class="bg_title"><h3 class="sidebar_header">Tweets</h3></div>
          <a class="twitter-timeline" href="https://twitter.com/AshleyJan" data-widget-id="472223170200489984" data-chrome="nofooter transparent noheader">Tweets by @AshleyJan</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div><!-- .widget_twitter -->
      </div>
    </aside>
  </div>
</div><!-- .pre_footer -->

<footer>
  <div class="footer_line container">
    <div class="socials">
      <ul class="socials_list">
        <li><a href="https://www.flickr.com/photos/43686204@N07/" class="ico_social-flickr"></a></li>
        <li><a href="http://instagram.com/ashleyjanm" class="ico_social-instagram"></a></li>
        <li><a href="https://twitter.com/AshleyJan" class="ico_social-twitter"></a></li>
      </ul>
    </div>
    <div class="copyright">&copy; AshleyJan.com. All Rights Reserved.</div>
    <div class="clear"></div>
  </div>
</footer>
<?php endif; ?>